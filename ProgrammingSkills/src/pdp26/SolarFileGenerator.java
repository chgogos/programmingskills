package pdp26;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Random;
import java.util.Scanner;

public class SolarFileGenerator {

	public static void main(String[] args) {
		System.out.println("1. Generate Random Values (Found)");
		System.out.println("2. Generate Random Values (ascending Found)");
		System.out.println("3. Generate Random Values (NotFound)");
		System.out.println("Enter choice");
		Scanner scanner = new Scanner(System.in);
		int choice = scanner.nextInt();
		int N = 1000000;
		if (choice == 1)
			generateFoundRandom(N);
		else if (choice==2)
			generateAscending(N);
		else
			generateNotFoundRandom(N);
		scanner.close();
	}

	private static void generateFoundRandom(int N) {
		int a[] = new int[N];

		Random random = new Random(123456L);
		for (int i = 0; i < N / 2; i++) {
			a[i] = random.nextInt(1000);
		}
		a[N / 2] = 1500;
		for (int i = N / 2 + 1; i < N; i++) {
			a[i] = 2000 + random.nextInt(1000);
		}

		BufferedWriter writer = null;
		try {
			File logFile = new File("pdp//solar.in");
			writer = new BufferedWriter(new FileWriter(logFile));
			writer.write(String.valueOf(N) + "\n");
			for (int i = 0; i < N; i++)
				writer.write(String.valueOf(a[i]) + "\n");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (Exception e) {
			}
		}
	}

	private static void generateAscending(int N) {
		BufferedWriter writer = null;
		try {
			File logFile = new File("pdp//solar_ascending.in");
			writer = new BufferedWriter(new FileWriter(logFile));
			writer.write(String.valueOf(N) + "\n");
			for (int i = 0; i < N; i++)
				writer.write(i + "\n");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (Exception e) {
			}
		}
	}

	private static void generateNotFoundRandom(int N) {
		int a[] = new int[N];

		Random random = new Random(123456L);
		for (int i = 0; i < N; i++) {
			a[i] = random.nextInt(1000);
		}
		BufferedWriter writer = null;
		try {
			File logFile = new File("pdp//solar.in");
			writer = new BufferedWriter(new FileWriter(logFile));
			writer.write(String.valueOf(N) + "\n");
			for (int i = 0; i < N; i++)
				writer.write(String.valueOf(a[i]) + "\n");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (Exception e) {
			}
		}
	}

}
