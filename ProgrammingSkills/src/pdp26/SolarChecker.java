package pdp26;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import com.google.common.base.Stopwatch;

public class SolarChecker {

	public static void main(String[] args) throws IOException {

		int a[];
		int N;

		System.out.println("Select file");
		Scanner scanner = new Scanner(System.in);
		File folder = new File("pdp");
		File[] listOfFiles = folder.listFiles();
		Arrays.sort(listOfFiles);
		int k= 1;
		for (File fileEntry : listOfFiles) {
			if (fileEntry.isFile()) {
				System.out.printf("%d %s\n",k, fileEntry.getAbsolutePath());
				k++;
			}
		}
		int choice = scanner.nextInt();
		System.out.println("1. Naive algorithm");
		System.out.println("2. Fast algorithm 1");
		System.out.println("3. Fast algorithm 2");
		System.out.print("Enter choice: ");
		int choice2 = scanner.nextInt();
		scanner.close();
				
		Stopwatch stopwatch = Stopwatch.createStarted();
		BufferedReader br = new BufferedReader(new FileReader(
				listOfFiles[choice-1]));
		
		try {
			String line = br.readLine();
			N = Integer.parseInt(line);
			a = new int[N];
			int i = 0;
			line = br.readLine();
			while (line != null) {
				a[i] = Integer.parseInt(line);
				line = br.readLine();
				i++;
			}
		} finally {
			br.close();
		}

		if (choice2==1)
			System.out.println("Result " + naive(a));
		else if (choice2==2)
			System.out.println("Result " + fast1(a));
		else
			System.out.println("Result " + fast2(a));
		
		// for (int i=0;i<a.length;i++)
		// System.out.println(a[i]);
		// long millis = stopwatch.elapsed(TimeUnit.MILLISECONDS);
		
		System.out.printf("time: " + stopwatch);
	}

	public static String naive(int[] a) {
		int max = Integer.MIN_VALUE;
		for (int i = 1; i < a.length - 1; i++) {
			int candidate = a[i];
			boolean flag = true;
			for (int j = i - 1; j >= 0; j--) {
				if (a[j] >= candidate) {
					flag = false;
					break;
				}
			}
			for (int j = i + 1; j < a.length; j++) {
				if (a[j] <= candidate) {
					flag = false;
					break;
				}
			}
			if (flag)
				if (candidate > max)
					max = candidate;
		}
		if (max == Integer.MIN_VALUE)
			return "NOT FOUND";
		else
			return String.valueOf(max);
	}

	public static String fast1(int[] a) {
		int wanted = Integer.MIN_VALUE;
		int max = a[1];
		List<Integer> aList = new ArrayList<>();
		for (int i = 1; i < a.length - 1; i++) {
			if ((a[i] > max) && (a[i] > wanted)) {
				wanted = a[i];
				// System.out.printf("possible wanted changed to %d\n", wanted);
				aList.add(wanted);
			}
			if (a[i] > max)
				max = a[i];
			while (!aList.isEmpty() && aList.get(aList.size() - 1) > a[i]) {
				aList.remove(aList.size() - 1);
			}
		}
		if (aList.isEmpty())
			return "NOT FOUND";
		else {
			System.out.println(aList);
			return String.valueOf(aList.get(0));
		}}

	public static String fast2(int[] a) {
		int wanted = Integer.MIN_VALUE;
		int max = a[1];
		int aList[] = new int[a.length];
		int top = 0;
		for (int i = 1; i < a.length - 1; i++) {
			if ((a[i] > max) && (a[i] > wanted)) {
				wanted = a[i];
				top++;
				aList[top] = wanted;
			}
			if (a[i] > max)
				max = a[i];
			while (top > 0 && aList[top] > a[i]) {
				top--;
			}
		}
		if (top == 0)
			return "NOT FOUND";
		else {
			while(aList[top]>=a[a.length-1]){
				top--;
			}
			return String.valueOf(aList[top]);
		}
	}

}
